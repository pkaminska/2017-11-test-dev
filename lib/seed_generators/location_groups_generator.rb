# encoding: UTF-8
module SeedGenerators
  class LocationGroupsGenerator < BaseGenerator
    
    # SeedGenerators::LocationGroupsGenerator.new.load
    def load(force=false)
      return if !force && LocationGroup.count > 0
      query = "TRUNCATE TABLE `location_groups`;"
      results = ActiveRecord::Base.connection.execute(query)

      4.times do |index|
        LocationGroup.create(name: "Group #{index+1}", country_id: (index%3 + 1), panel_provider_id: (index%3 + 1))
      end
      
      p "#{self.class} loaded"
    end

  end
end