# encoding: UTF-8
module SeedGenerators
  class LocationsGenerator < BaseGenerator

    def names
      ['Amsterdam', 'Barcelona', 'Cracow', 'Dartmoor', 'Dniepr', 'Etna Mt.', 'Fuji-san', 'Geneva', 'Hamburg', 'Transylvania', 'Toscany', 'Provence', 'Boston', 'London', 'Paris', 
        'Ranya', 'Great Wall of China', "Giza", 'Navarre', "Alpes", ]
    end
      

    # SeedGenerators::LocationsGenerator.new.load
    def load(force=false)
      return if !force && Location.count > 0
      query = "TRUNCATE TABLE `locations`;"
      results = ActiveRecord::Base.connection.execute(query)
      query = "TRUNCATE TABLE `location_location_groups`;"
      results = ActiveRecord::Base.connection.execute(query)

      names.each_with_index do |n, index|
        index += 1
        Location.create(name: n, external_id: rand(1000*index..1000*(index+1)), location_group_ids: ["#{rand(1..2) }", "#{rand(3..4)}"] )
      end
        
      p "#{self.class} loaded"
    end


  end
end