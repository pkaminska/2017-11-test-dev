# encoding: UTF-8
module SeedGenerators
  class TargetGroupsGenerator < BaseGenerator

    # SeedGenerators::TargetGroupsGenerator.new.load
    def load(force=false)
      return if !force && TargetGroup.count > 0
      query = "TRUNCATE TABLE `target_groups`;"
      results = ActiveRecord::Base.connection.execute(query)
      query = "TRUNCATE TABLE `country_target_groups`;"
      results = ActiveRecord::Base.connection.execute(query)

      TargetGroup.create(name: "Root Target Group 1", panel_provider_id: 1, country_ids: ["1", "2"])
      TargetGroup.create(name: "Root Target Group 2", panel_provider_id: 2, country_ids: ["2", "3"])
      TargetGroup.create(name: "Root Target Group 3", panel_provider_id: 3, country_ids: ["1", "3"])
      TargetGroup.create(name: "Root Target Group 4", panel_provider_id: 1, country_ids: ["1", "3"])
      
      roots = TargetGroup.at_depth(0)
      roots.each_with_index do |n, index|
        t = TargetGroup.create(name: "First-deep Target Group #{index+1}1", panel_provider_id: rand(1..3), parent: n, country_ids: ["1", "2"])
        puts t.errors.inspect if t.errors.any?
        TargetGroup.create(name: "First-deep Target Group #{index+1}2", panel_provider_id: rand(1..3), parent: n)
      end
      
      first_deeps = TargetGroup.at_depth(1)
      first_deeps.each_with_index do |n, index|
        TargetGroup.create(name: "Second-deep Target Group #{index+1}1", panel_provider_id: rand(1..3), parent: n)
        TargetGroup.create(name: "Second-deep Target Group #{index+1}2", panel_provider_id: rand(1..3), parent: n)
        TargetGroup.create(name: "Second-deep Target Group #{index+1}3", panel_provider_id: rand(1..3), parent: n)
      end
      
      second_deeps = TargetGroup.at_depth(2)
      second_deeps.each_with_index do |n, index|
        TargetGroup.create(name: "Third-deep Target Group #{index+1}1", panel_provider_id: rand(1..3), parent: n)
        TargetGroup.create(name: "Third-deep Target Group #{index+1}2", panel_provider_id: rand(1..3), parent: n)
        TargetGroup.create(name: "Third-deep Target Group #{index+1}3", panel_provider_id: rand(1..3), parent: n)
        TargetGroup.create(name: "Third-deep Target Group #{index+1}4", panel_provider_id: rand(1..3), parent: n)
      end
      
      third_deeps = TargetGroup.at_depth(3)
      third_deeps.each_with_index do |n, index|
        TargetGroup.create(name: "Fourth-deep Target Group #{index+1}1", panel_provider_id: rand(1..3), parent: n)
        TargetGroup.create(name: "Fourth-deep Target Group #{index+1}2", panel_provider_id: rand(1..3), parent: n)
        TargetGroup.create(name: "Fourth-deep Target Group #{index+1}3", panel_provider_id: rand(1..3), parent: n)
        TargetGroup.create(name: "Fourth-deep Target Group #{index+1}4", panel_provider_id: rand(1..3), parent: n)
        TargetGroup.create(name: "Fourth-deep Target Group #{index+1}5", panel_provider_id: rand(1..3), parent: n)
      end
        
      p "#{self.class} loaded"
    end


  end
end