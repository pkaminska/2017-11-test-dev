# encoding: UTF-8
module SeedGenerators
  class CountriesGenerator < BaseGenerator


    def names
      [
        ['AL', 'Albania', 1],
        ['BI', 'Birma', 2],
        ['CR','Croatia', 3]
      ]
    end

    # SeedGenerators::PlacesGenerator.new.load
    def load(force=false)
      return if !force && Country.count > 0
      query = "TRUNCATE TABLE `countries`;"
      results = ActiveRecord::Base.connection.execute(query)

      names.each_with_index do |n, index|
        index += 1
        Country.create(name: n[1], country_code: n[0], panel_provider_id: n[2])
      end

      p "#{self.class} loaded"
    end


  end
end