# encoding: UTF-8
module SeedGenerators
  class UsersGenerator < BaseGenerator

    def names
      ['Tom P.', 'Sylwia', 'Johanson', 'Olivier S.', 'Marry', 'Criss', 'Zuza', 'Zosia']
    end

    def create_test_item(options={})
      User.create(
        email: options[:email].nil? ? "test#{User.count+1}@helloweb.pl" : options[:email],
        password: 'test123',
        password_confirmation: 'test123',
        # admin: options[:admin].nil? ? false : options[:admin]
      )
    end

    # SeedGenerators::PlacesGenerator.new.load
    def load(force=false)
      return if !force && User.count > 0
      query = "TRUNCATE TABLE `users`;"
      results = ActiveRecord::Base.connection.execute(query)

      create_test_item(email: 'p.t.kaminska@gmail.com' )
      create_test_item(email: 'test_user@example.com' )

      p "#{self.class} loaded"
      
      User.all.each { |u| u.activate! }
      p "#{self.class} activated"
    end


  end
end