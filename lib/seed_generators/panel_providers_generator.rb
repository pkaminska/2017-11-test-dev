# encoding: UTF-8
module SeedGenerators
  class PanelProvidersGenerator < BaseGenerator
    
    def data 
      [
        ['HtmlTagPanelProvider', 'http://time.com', 'a'],
        ['HtmlSignPanelProvider', 'http://time.com', 'a'],
        ['JsonReqPanelProvider', 'http://openlibrary.org/search.json?q=the+lord+of+the+rings', nil]
      ]
    end
    
    # SeedGenerators::PanelProvidersGenerator.new.load
    def load(force=false)
      return if !force && PanelProvider.count > 0
      query = "TRUNCATE TABLE `panel_providers`;"
      results = ActiveRecord::Base.connection.execute(query)

      data.each do |d|
        PanelProvider.create(code: d[0], external_url: d[1], selected_tag: d[2])
      end

      p "#{self.class} loaded"
    end


  end
end