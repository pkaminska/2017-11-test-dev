# encoding: UTF-8
module SeedGenerators
  class BaseGenerator

    def dragonfly
      @dragonfly = Dragonfly.app
    end

    def rand_true
      rand(2)==0
    end
  end
end