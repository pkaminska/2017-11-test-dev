Rails.application.routes.draw do

  root 'locations#index'
  
  get     'login'   => 'sessions#new',      as: :login
  post    'login'   => 'sessions#create',   as: :create_login
  match   'logout'  => 'sessions#destroy',  as: :logout, via: [:get, :delete]
  
  resources :registrations, only: [:new, :create] do
    get 'activate', on: :member
  end

  # public
  resources :locations, only: :index
  get  'locations/:country_code', to: 'locations#index'
  get  'target_groups/:country_code', to: 'target_groups#index'
  
  # private 
  get 'panel' => redirect('panel/locations')
  post 'evaluate_target' => 'panel/locations#evaluate_target'
  namespace :panel, path: 'panel' do
    resources :locations, only: :index
    get  'locations/:country_code', to: 'locations#index'
    get  'target_groups/:country_code', to: 'target_groups#index'
  end
  
end
