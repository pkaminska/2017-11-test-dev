class UserMailer < ApplicationMailer
  
  def activation_needed_email(user)
    @user = user
    @url  = activate_registration_url(@user.activation_token)
    mail(:to => user.email,
      :subject => 'Welcome')
  end  

  def activation_success_email(user)
    @user = user
    @url  = login_url
    mail(:to => user.email,
      :subject => 'You activated your profile succesfully')
  end  
  
end
