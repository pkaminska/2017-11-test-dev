class User < ActiveRecord::Base
  authenticates_with_sorcery!
  
  validates :email, presence: true
  validates :email, uniqueness: { case_sensitive: false, message: "E-mail is already in our base" }
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
  validates :password, length: { minimum: 3 }, if: -> { new_record? || changes["password"] }
  validates :password, confirmation: true, if: -> { new_record? || changes["password"] }
  validates :password_confirmation, presence: true, if: -> { new_record? || changes["password"] }
  
  ### methods
  def pending?
    activation_state == 'pending'
  end

  def active?
    activation_state == 'active'
  end
end
