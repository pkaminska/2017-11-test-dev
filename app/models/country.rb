class Country < ActiveRecord::Base
  extend FriendlyId
  friendly_id :country_code, use: :slugged
  
  ### associations
  belongs_to :panel_provider
  has_many :location_groups
  has_many :locations, -> { distinct }, through: :location_groups #unused
  has_many :country_target_groups, dependent: :destroy
  has_many :target_groups, -> { distinct }, through: :country_target_groups
  
  ### validations
  validates :name, presence: true, uniqueness: true
  validates :country_code, presence: true, uniqueness: true
  validates :panel_provider_id, presence: true
  
  ### methods
  def self.countries_list
    self.all.map { |c| [c.name, c.country_code]}
  end
  
end
