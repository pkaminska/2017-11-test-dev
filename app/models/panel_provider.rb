require 'json'
class PanelProvider < ActiveRecord::Base
  CODE_TYPES = %w'HtmlTagPanelProvider HtmlSignPanelProvider JsonReqPanelProvider'
  
  ### associations
  # has_many :countries
    
  ### validations
  validates_inclusion_of :code, in: CODE_TYPES
  validates :external_url, presence: true # can be replaced by default value or hardcoded in accordance with PanelProvider.code
    
  ### scopes
  
  ### methods
  
  def set_type
    raise "No 'set_type' method redefined in inheriting model"
  end
  
  # types
  def html_tag?
    code == 'HtmlTagPanelProvider'
  end
  
  def html_sign?
    code == 'HtmlSignPanelProvider'
  end
  
  def json_req?
    code == 'JsonReqPanelProvider'
  end
  
  def price_evaluation(response)
    if self.html_tag?
      price = selected_tag.present? ? response.css(selected_tag).count : response.css(a).count
      price = price / 100.00
    elsif self.html_sign?
      price = selected_tag.present? ? response.text.scan(selected_tag).count : response.text.scan("a").count
      price = price / 100.00
    else
      price = count_long_arrays(response)
      price = price.size
    end
    return price
  end
  
  ## could be packed to module if used by other classes
  def to_add?(element)
    element.is_a?(Array) && element.count >= 10
  end
  
  def count_long_arrays(response, i = 0)
    logger.debug JSON[response]
    element = JSON[response]
    logger.debug element.class
    i << element if to_add?(element)
    logger.debug i
    
    if element.is_a?(Array) || element.is_a?(Hash)
      element.each do |n|
        i << n if to_add?(n)
        if n.is_a?(Hash) || n.is_a?(Array)
          i << count_long_arrays(n, i)
        end
      end
    end
    logger.debug i.size
    return i
  end
  
end
