class LocationLocationGroup < ActiveRecord::Base
  belongs_to :location
  belongs_to :location_group
  
  validates :location_id, uniqueness: { scope: :location_group_id }
end
