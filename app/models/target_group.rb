class TargetGroup < ActiveRecord::Base
  has_ancestry orphan_strategy: :adopt, cache_depth: true 
  before_validation :generate_secret_code
  before_save :remove_country_ids_from_deeper_nodes, if: :should_be_cleared?
  
  ### associations
  has_many :country_target_groups, dependent: :destroy
  has_many :countries, through: :country_target_groups
  belongs_to :panel_provider
  
  ### validations
  validates :name, presence: true, uniqueness: true
  validates :panel_provider_id, presence: true
  
  ### scopes
  scope :sorted, -> { order 'id ASC' }
  scope :all_with_proper_provider, ->(provider_id) { where(panel_provider_id: provider_id ) } # only for collection
  
  ### methods
  def generate_secret_code
    self.secret_code = rand(100..999).to_s+[*('a'..'z')][rand(1..25)]+[*('a'..'z')][rand(1..25)]+rand(100..999).to_s if self.secret_code.blank?
  end

  def should_be_cleared?
    !self.root? && self.country_ids.any?
  end

  def remove_country_ids_from_deeper_nodes
    self.country_ids = nil
  end
  
  def self.targets_list
    self.all.map { |c| [c.name, c.id] }
  end
  
  # arrange ancestry subtree
  def self.arrange_as_array(options = {}, hash = nil)
    hash ||= arrange(order: :id)

    arr = []
    hash.each do |node, children|
      arr << node
      arr += arrange_as_array(options, children) unless children.empty?
    end
    arr
  end
  
end
