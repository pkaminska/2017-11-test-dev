class Location < ActiveRecord::Base
  before_validation :generate_secret_code
  
  ### associations
  has_many :location_location_groups, dependent: :destroy
  has_many :location_groups, through: :location_location_groups
  
  ### validations
  validates :external_id, presence: true, uniqueness: true
  validates :secret_code, presence: true
  validates :name, presence: true
  
  ### scopes
  scope :sorted, -> { order 'id ASC' }
      
  ### methods
  def generate_secret_code
    self.secret_code = [*('a'..'z')][rand(1..25)]+rand(100..999).to_s+[*('a'..'z')][rand(1..25)]+rand(100..999).to_s if self.secret_code.blank?
  end
  
end
