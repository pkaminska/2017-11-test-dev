class LocationGroup < ActiveRecord::Base
  has_many :location_location_groups, dependent: :destroy
  has_many :locations, -> { distinct }, through: :location_location_groups
  belongs_to :panel_provider
  
  ### validations ###
  validates :name, presence: true
  validates :panel_provider_id, presence: true
  validates :country_id, presence: true
  
  ### scopes
  scope :sorted, -> { order 'id ASC' }
  scope :all_with_proper_provider, ->(provider_id) { where(panel_provider_id: provider_id ) } # only for collection
end
