class CountryTargetGroup < ActiveRecord::Base
  belongs_to :country
  belongs_to :target_group
  
  validates :country_id, uniqueness: { scope: :target_group_id }
end
