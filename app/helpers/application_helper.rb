module ApplicationHelper
  
  def countries_list(l = 'locations', country)
    if @panel
      link_to "#{country.name} (#{country.country_code})", "/panel/#{l}/#{country.country_code}", class: 'btn locations-js', remote: true
    else
      link_to "#{country.name} (#{country.country_code})", "/#{l}/#{country.country_code}", class: 'btn locations-js', remote: true
    end
  end
  
end
