class RegistrationsController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(permitted_params)
    if @user.save
      redirect_to(root_path, flash: { success: "You've been registered. Check your email to activate account" })
    else
      flash.now[:alert] = "Please check form for mistakes"
      render :new
    end
  end

  def activate
    if (@user = User.load_from_activation_token(params[:id]))
      @user.activate!
      redirect_to(login_path, flash: { success: 'Account was activated. You may log in.' })
    else
      redirect_to(root_path, flash: { alert: "We're sorry, no such request" })
    end
  end

  private

  def permitted_params
    params[:user].permit(:email, :password, :password_confirmation)
  end
end
