class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
    
  private 
  def user_required
    redirect_to(login_path, alert: 'Please log in to proceed') unless current_user
  end
end
