class TargetGroupsController < ApplicationController
  
  def index
    if params[:country_code] 
      if @country = Country.friendly.find(params[:country_code])
        @target_groups = @country.target_groups
        respond_to { |format| format.js { render 'target_groups/show_target_groups' } }
      else
        respond_to { |format| format.js { text 'locations/error' } }
      end
    end
  end
  
end
