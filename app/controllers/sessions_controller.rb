class SessionsController < ApplicationController
  before_action :no_user, only: %i[new create]
  before_action :user_required, only: :destroy
  
  def new
    @user = User.new
  end

  def create
    @user = User.find_by(email: params[:email])
    if @user && @user.pending?
      redirect_to(login_path(email: params[:email]), alert: "You need to activate your account")
    elsif @user = login(params[:email], params[:password])
      redirect_to(panel_path, flash: { success: 'You were logged in' })
    else
      flash.now[:alert] = 'Wrong data'
      render action: 'new'
    end
  end

  def destroy
    logout
    redirect_to root_path, flash: { success: 'You were logged out' }
  end
  
  private
  def no_user
    redirect_to panel_path if current_user
  end
end
