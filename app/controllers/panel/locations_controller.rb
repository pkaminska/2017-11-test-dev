require 'open-uri'

class Panel::LocationsController < PanelController
  
  def index
    @countries = Country.all
    @all_locations = Location.all
    if params[:country_code]
      if @country = Country.friendly.find(params[:country_code])
        @location_groups = @country.location_groups.all_with_proper_provider(@country.panel_provider.id)
        @locations = @country.locations
        respond_to { |format| format.js { render 'show_locations' } }
      else
        respond_to { |format| format.js { text 'locations/error' } }
      end
    else
      render 'locations/index'
    end
  end
  
  def evaluate_target
    country = Country.friendly.find(params[:country_code]) if params[:country_code]
    panel_provider = country.panel_provider
    target_group = TargetGroup.find(params[:target_group_id]) if params[:target_group_id]
    if params[:location_ids]
      params[:location_ids].each do |l|
        Location.find(l.to_i)
      end
    end
    response = Nokogiri::HTML(open(panel_provider.external_url.freeze).read)
    @price = panel_provider.price_evaluation(response)

    respond_to { |format| format.js }
    
  rescue
    redirect_to(panel_path, flash: { alert: "We're sorry, something went wrong" })
  end
end
