class LocationsController < ApplicationController
  
  def index
    @countries = Country.all
    if params[:country_code] 
      if @country = Country.friendly.find(params[:country_code])
        @location_groups = @country.location_groups.all_with_proper_provider(@country.panel_provider.id)
        @locations = @country.locations
        respond_to { |format| format.js { render 'show_locations' } }
      else
        respond_to { |format| format.js { text 'locations/error' } }
      end
    end
  end
end
