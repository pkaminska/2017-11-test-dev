class PanelController < ApplicationController
  before_action :set_panel_variable
  before_action :user_required
  
  private
  def set_panel_variable
    @panel = 'panel'
  end
end
