class CreatePanelProviders < ActiveRecord::Migration
  def change
    create_table :panel_providers do |t|
      t.string :code
      
      t.string :external_url
      t.string :selected_tag

      t.timestamps null: false
    end
  end
end
