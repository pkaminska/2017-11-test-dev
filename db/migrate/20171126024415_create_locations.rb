class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.integer :external_id, null: false
      t.string :secret_code

      t.timestamps null: false
    end
    add_index :locations, :external_id, unique: true
  end
end
