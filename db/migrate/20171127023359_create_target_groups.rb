class CreateTargetGroups < ActiveRecord::Migration
  def change
    create_table :target_groups do |t|
      t.string :name
      t.integer :panel_provider_id
      t.string :ancestry
      t.integer :ancestry_depth, default: 0
      t.string :secret_code

      t.timestamps null: false
    end
    add_index :target_groups, :ancestry
  end
end
