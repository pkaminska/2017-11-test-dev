class CreateLocationLocationGroups < ActiveRecord::Migration
  def change
    create_table :location_location_groups do |t|
      t.belongs_to :location, index: true
      t.belongs_to :location_group, index: true
      
      t.timestamps null: false
    end
  end
end
