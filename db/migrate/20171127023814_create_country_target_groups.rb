class CreateCountryTargetGroups < ActiveRecord::Migration
  def change
    create_table :country_target_groups do |t|
      t.belongs_to :country, index: true
      t.belongs_to :target_group, index: true
    end
  end
end
