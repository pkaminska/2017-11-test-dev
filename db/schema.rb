# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171127023814) do

  create_table "countries", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.string   "country_code",      limit: 255
    t.integer  "panel_provider_id", limit: 4
    t.string   "slug",              limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "country_target_groups", force: :cascade do |t|
    t.integer "country_id",      limit: 4
    t.integer "target_group_id", limit: 4
  end

  add_index "country_target_groups", ["country_id"], name: "index_country_target_groups_on_country_id", using: :btree
  add_index "country_target_groups", ["target_group_id"], name: "index_country_target_groups_on_target_group_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, length: {"slug"=>70, "sluggable_type"=>nil, "scope"=>70}, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", length: {"slug"=>140, "sluggable_type"=>nil}, using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "location_groups", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.integer  "country_id",        limit: 4
    t.integer  "panel_provider_id", limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "location_location_groups", force: :cascade do |t|
    t.integer  "location_id",       limit: 4
    t.integer  "location_group_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "location_location_groups", ["location_group_id"], name: "index_location_location_groups_on_location_group_id", using: :btree
  add_index "location_location_groups", ["location_id"], name: "index_location_location_groups_on_location_id", using: :btree

  create_table "locations", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.integer  "external_id", limit: 4,   null: false
    t.string   "secret_code", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "locations", ["external_id"], name: "index_locations_on_external_id", unique: true, using: :btree

  create_table "panel_providers", force: :cascade do |t|
    t.string   "code",         limit: 255
    t.string   "external_url", limit: 255
    t.string   "selected_tag", limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "target_groups", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.integer  "panel_provider_id", limit: 4
    t.string   "ancestry",          limit: 255
    t.integer  "ancestry_depth",    limit: 4,   default: 0
    t.string   "secret_code",       limit: 255
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "target_groups", ["ancestry"], name: "index_target_groups_on_ancestry", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                       limit: 255, null: false
    t.string   "crypted_password",            limit: 255
    t.string   "salt",                        limit: 255
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "activation_state",            limit: 255
    t.string   "activation_token",            limit: 255
    t.datetime "activation_token_expires_at"
  end

  add_index "users", ["activation_token"], name: "index_users_on_activation_token", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

end
