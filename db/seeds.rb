# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


SeedGenerators::UsersGenerator.new.load(true)

SeedGenerators::CountriesGenerator.new.load(true)
SeedGenerators::PanelProvidersGenerator.new.load(true)

SeedGenerators::LocationGroupsGenerator.new.load(true)
SeedGenerators::LocationsGenerator.new.load(true)
SeedGenerators::TargetGroupsGenerator.new.load(true)